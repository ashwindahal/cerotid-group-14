package GeicoPagesTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import GeicoPages.BundlePage;
import GeicoPages.GlobalElements;
import GeicoPages.HomePage;
import SeleniumUtils.GlobalMethods;
import SeleniumUtils.ScreenShotUtility;

public class StartInsuranceQuote {

	WebDriver driver;

	@org.testng.annotations.BeforeTest
	public void BeforeTest() {
		// set system path
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	public void navigateToHomePage() {

		try {
			// Required Objects for this method
			HomePage homePage = new HomePage(driver);
			ScreenShotUtility util = new ScreenShotUtility();

			driver.get("https://www.geico.com/");
			driver.manage().window().maximize();

			// waite for page load
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Thread.sleep(5000);

			// Check if page is Opened
			Assert.assertTrue(homePage.isPageOpenend());

			// Take A Screenhot of the Page
			util.takeSnopShot(driver, "Geico Home Page");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test(priority = 0)
	public void StartAutoQuote() {
		// Navigate to home page
		navigateToHomePage();

		try {
			HomePage homePage = new HomePage(driver);
			BundlePage bp = new BundlePage(driver);
			GlobalElements ge = new GlobalElements(driver);
			ScreenShotUtility util = new ScreenShotUtility();
			GlobalMethods gm = new GlobalMethods();
			
			//Test Here
			gm.ScrollToElement(driver, GlobalElements.contactUs);
			Thread.sleep(10000);
			
			
			//--Enter Zip
			homePage.enterZip("75150");
			Thread.sleep(3000);
			
			//--Click on Start Quote 
			homePage.clickStartAQuote();
			Thread.sleep(5000);
			util.takeSnopShot(driver, "Bundle Page");

			//Bundle Page 
			//Click next
			bp.nextBtn();
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	
	
	
	@Test(priority = 1)
	public void StartRenters() {
		navigateToHomePage();
		
		
		HomePage homePage = new HomePage(driver);
		ScreenShotUtility util = new ScreenShotUtility();
		
		homePage.rentersQuoteFlow("75150");
		util.takeSnopShot(driver, "Renters Next Page");

	}
	
	
	@Test(priority = 2)
	public void GoogleTest() throws InterruptedException {
		
		ScreenShotUtility util = new ScreenShotUtility();
		

		driver.get("https://www.google.com/");
		driver.manage().window().maximize();

		// waite for page load
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Thread.sleep(5000);
		
		
		if (driver.getTitle().contains("Google")) {
			System.out.println("Pass: Correct Page was Openend");	
		}
		
		// Take A Screenhot of the Page
		util.takeSnopShot(driver, "Google Home Page");
		
	}
	
	
	@AfterTest
	public void AfterTest() {
		//Close the browser;
		//driver.close();
		//Quite the Driver 
		//driver.quit();
	}

}
