package Tests;

import java.util.ArrayList;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import Pages.CerotidPage;

public class CerotidPageTest {

	static WebDriver driver;

	public static void main(String[] args) {

		// Step 1: Invoke the Browser and navigate to cerotid homepage
		invokeBrowser();
		// Step 2: Fill Form
		fillForm();
		// Step 3: Terminate Browser
		terminateBrowser();

	}

	public static void invokeBrowser() {
		// Step 1: Set the System Path
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		// Step 2: Create an instance of Webdriver of type chrome
		driver = new ChromeDriver();
		// Step 3:
		driver.get("https://www.cerotid.com");
		// Step 4: Maximize the Screen
		driver.manage().window().maximize();

		// Switch to second tab
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));

		// Validate if Correct Page is Loaded
		String pageTitle = driver.getTitle();

		if (pageTitle.equals("Cerotid")) {
			System.out.println(pageTitle + " -------------------------------- was launched as expected");
		} else {
			System.out.println("Invalid Page " + pageTitle + " Was launched");
		}

	}

	public static void fillForm() {
		try {

			// Utilizing the cerotidpage Objects/Elements to select course
			Select chooseCourse = new Select(CerotidPage.selectCourse(driver));
			chooseCourse.selectByIndex(3);

			// Utilizing the cerotidpage Objects/Elements to select session
			Select chooseSession = new Select(CerotidPage.selectSession(driver));
			chooseSession.selectByValue("NewSession");

			// Utilizing the cerotidpage Objects/Elements to enter Full Name
			CerotidPage.fullName(driver).sendKeys("Test Tester");

			// Utilizing the cerotidpage Objects/Elements to enter Address
			CerotidPage.addressField(driver).sendKeys("123 Test St.");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void terminateBrowser() {
		/*
		 * // will close windows driver.close(); // Quit Session driver.quit();
		 */
	}

}
