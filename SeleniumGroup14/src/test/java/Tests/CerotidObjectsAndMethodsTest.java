package Tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import Pages.CerotidObjectsAndMethods;
import SeleniumUtils.ScreenShotUtility;

public class CerotidObjectsAndMethodsTest {
	
	static WebDriver driver;

	public static void main(String[] args) {
		
		//1: Invoke The Browser 
		invokeBrowser();
		
		//2: fill from 
		fillForm();
		
		//3: Terminate the browser 
		terminateBrowser();		
	}

	public static void terminateBrowser() {
		//Will close all windows 
		driver.close();
		
		//Quit Session
		driver.quit();	
	}

	public static void fillForm() {
		ScreenShotUtility screenshot = new ScreenShotUtility();
		CerotidObjectsAndMethods cerotidForm = new CerotidObjectsAndMethods(driver);
		cerotidForm.selectCourse(5);
		screenshot.takeSnopShot(driver,"Cerotid_Empty_Form");
		cerotidForm.selectSession("NewSession");
		cerotidForm.enterFullName("Test Tester");
		cerotidForm.enterAddress("123 Test Address");
		screenshot.takeSnopShot(driver,"Cerotid_Compleated_Form");
	}

	public static void invokeBrowser() {
		
		//1: Set System Path 
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		
		//2: Chrome Driver OBJ/Instance
		driver = new ChromeDriver();
		
		//3: Navigate to Cerotid Page
		driver.get("https://www.cerotid.com/");
		
		if (driver.getTitle().equals("Cerotid")) {
			System.out.println("Cerotid Was Lauched--------------");
		}else {
			System.out.println("Wrong Page");
		}
		
		//Max the Screen
		driver.manage().window().maximize();
		
	}

}
