package properties;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class PropertiesConfigTestNgDemo {

	public WebDriver driver;

	// Store value of browser
	public static String browserName = null;
	public static String userName = null;
	public static String passWord = null;

	@BeforeTest
	public void setUpTest() {
		// Read config file and invoke browser basesd on the configuration
		PropertiesConfiguration.getProperties();

		if (userName.contains("user") && passWord.contains("password")) {

			if (browserName.contains("chrome")) {
				System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
				driver = new ChromeDriver();
			} else if (browserName.contains("firefox")) {
				System.setProperty("webdriver.gecko.driver", ".\\libs\\geckodriver.exe");
				driver = new FirefoxDriver();
			}

		} else {
			System.out.println("Invalid Cerdentials");
		}

	}

	@Test
	public void googleTest() {

		// Step 3
		// Utilize the driver and invoke the chrome browser
		// Visit a Page
		driver.get("https://www.google.com");

		// Max the Screen or browser
		driver.manage().window().maximize();

		// Step 4
		// Find a element
		// Creating Webelement onject and by locating it with the driver

		// Search Field
		WebElement searchField = driver.findElement(By.xpath("//input[@title='Search']"));

		// Step 5
		// Work with a Found Element
		searchField.click(); // clicks on element
		// searchField.submit(); // submits a form
		// searchField.clear(); // clears an input field of its text
		searchField.sendKeys("What is Selenium"); // Types Text into the input field

		// Ask A Question
		// Returns True/False value aka Boolean
		searchField.isDisplayed(); // Is it Visible to the Human Eye
		searchField.sendKeys(Keys.RETURN);

	}

	@AfterTest
	public void after_Test() {
		System.out.println("Terminating The Browser");
		// driver.close();
		PropertiesConfiguration.setProperties();
	}

}
