package properties;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public class PropertiesConfiguration {

	// 1: Create a Object of Properties
	static Properties prop = new Properties();

	// 2: Get the system project path
	static String projectPath = System.getProperty("user.dir");

	public static void main(String[] args) {
		// 1: Get the Current Properties
		getProperties();
		setProperties();
		getProperties();
	}

	public static void getProperties() {
		try {

			// 1: Create an object of inputstream to retrive/Read value from
			// config.properties file
			InputStream input = new FileInputStream(projectPath + "/src/main/java/properties/config.properties");
			// 2: Load the properties File
			prop.load(input);
			// 3 Get Vlaues from the Properties File
			String browser = prop.getProperty("browser");
			String userName = prop.getProperty("Username");
			String passWord = prop.getProperty("Password");

			

			System.out.println(browser + " Is Invoked");
			PropertiesConfigTestNgDemo.browserName = browser; 
			PropertiesConfigTestNgDemo.userName = userName; 
			PropertiesConfigTestNgDemo.passWord = passWord; 


		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void setProperties() {
		
		try {
			
			//1: Create a object of Output to write to the config file 
			OutputStream output = new FileOutputStream(projectPath + "/src/main/java/properties/config.properties");
			//2: Set new property with the key and value 
			prop.setProperty("browser", "chrome");
			prop.setProperty("Result", "Pass");
			//3: save/Store the value in the file 
			prop.store(output, "Storing/Updating config.properties File");	
			
		}catch (Exception e) {
			e.printStackTrace();
		}

	}

}
