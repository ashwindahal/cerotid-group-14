package ForntierHomeWork;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FronterArilines {

	public static void main(String[] args) {

		try {
			// 1: Set System Path
			System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");

			// Chrome driver obj
			WebDriver driver = new ChromeDriver();

			// Navigate to Frontier Website
			driver.get("https://www.flyfrontier.com");

			// Switch to second tab
			ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs.get(1));

			// Wait for Page to Load
			Thread.sleep(5000);

			// Start Filling form

			// Click on From Filed enter DFW Key Down press Enter
			WebElement from = driver.findElement(By.xpath("//input[@id='origin']"));
			from.click();
			from.clear();
			from.sendKeys("DFW");
			from.sendKeys(Keys.ARROW_DOWN);

			// City
			WebElement city = driver.findElement(By.xpath("//div[@class='city']//span"));
			city.click();
			from.sendKeys(Keys.TAB);

			WebElement to = driver.findElement(By.xpath("//input[@id='destination']"));
			to.sendKeys("LAX");
			Thread.sleep(2000);
			to.sendKeys(Keys.ARROW_DOWN);
			to.sendKeys(Keys.RETURN);
			to.sendKeys(Keys.TAB);

			// Departure Date
			WebElement departDateField = driver.findElement(By.xpath("//input[@id='departureDate']"));
			departDateField.click();

			// Click Arrow to get to july
			WebElement calenderNextRightBtn1 = driver.findElement(By.xpath("//a[@title='Next']//span"));
			calenderNextRightBtn1.click();
			Thread.sleep(2000);
			WebElement calenderNextRightBtn2 = driver.findElement(By.xpath("//a[@title='Next']//span"));
			calenderNextRightBtn2.click();

			// Select Date
			WebElement fromDate = driver.findElement(By.xpath("(//a[contains(@href,'#')])[54]"));
			fromDate.click();

			Thread.sleep(2000);

			WebElement toDate = driver.findElement(By.xpath("(//a[contains(@href,'#')])[17]"));
			toDate.click();

			// Add Travelers
			WebElement travelersField = driver.findElement(By.xpath("//input[@id='passengersInput']"));
			travelersField.click();

			WebElement addAdultBtn = driver.findElement(By.xpath("//img[@alt='add adult']"));
			addAdultBtn.click();
			Thread.sleep(2000);

			WebElement generalArea = driver.findElement(By.xpath("//div[@class='booking-widget']"));
			generalArea.click();

			// Search
			WebElement searchBtn = driver.findElement(By.xpath("//a[@id='btnSearch']"));
			searchBtn.click();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
