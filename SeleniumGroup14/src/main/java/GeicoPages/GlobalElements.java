package GeicoPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GlobalElements {

	// Create an instance/Object of Webdriver
	public WebDriver driver;

	// Constructor to create an object of this call
	public GlobalElements(WebDriver driver) {
		this.driver = driver;

		// Initilize the webelemnts for this class
		PageFactory.initElements(driver, this);
	}

	// Locating all elements that we are going to use from the the home page

	// Annotation
	@FindBy(xpath = "//*[@id=\"footer-links-secondary\"]/ul/li[5]/a")
	public static WebElement contactUs;


}
