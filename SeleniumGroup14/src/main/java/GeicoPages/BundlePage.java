package GeicoPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BundlePage {

	// Create an instance/Object of Webdriver
	public WebDriver driver;

	// Constructor to create an object of this call
	public BundlePage(WebDriver driver) {
		this.driver = driver;

		// Initilize the webelemnts for this class
		PageFactory.initElements(driver, this);
	}

	// Locating all elements that we are going to use from the the home page

	// Annotation
	@FindBy(xpath = "//span[text()='NEXT']/..")
	WebElement bundleNext;
	
	
	public void nextBtn() {
		bundleNext.click();
	}
	

}
