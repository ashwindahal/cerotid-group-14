package GeicoPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
	
	//Create an instance/Object of Webdriver 
	public WebDriver driver;
	
	//Constructor to create an object of this call 
	public HomePage(WebDriver driver) {
		this.driver = driver;
		
		//Initilize the webelemnts for this class
		PageFactory.initElements(driver, this);	
	}
	
	//Locating all elements that we are going to use from the the home page 
	
	//Annotation 
	@FindBy(xpath="//input[@id='zip']")
	WebElement homePageZipField; 
	
	@FindBy(xpath="//button[text()='Start Quote']")
	WebElement homePageStartQuoteBtn; 
	
	@FindBy(xpath="//div[@class='renters card']")
	WebElement homePageRentersCard; 

	
	//Methods that utilize the weblements from this page 
	
	//Zip Field
	public void enterZip(String zip) {
		homePageZipField.sendKeys(zip);
	}
	
	//Start a Quote Btn 
	public void clickStartAQuote() {
		homePageStartQuoteBtn.click();
	}
	
	public void rentersQuoteFlow(String zip) {
		homePageRentersCard.click();
		homePageZipField.sendKeys(zip);
		homePageStartQuoteBtn.click();
	}
	
	//Check and see if the page is opened
	public boolean isPageOpenend() {
		//Assert true/False if a page is loaded (Return true based on the condition of the title)
		return driver.getTitle().contains("An Insurance Company For Your Car And More | GEICO");
		
	}
}
