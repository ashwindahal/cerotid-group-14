package LinerAutomation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class GoogleTest {

	public static void main(String[] args) {

		// Step 1
		// Setting System Path --> to point to the chromedriver.exe file
		// Windows
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		// Mac
		// System.setProperty("webdriver.chrome.driver", "./libs/chromedriver");

		// Step 2
		// Create an instance of webdriver and morphing it to chrome
		WebDriver driver = new ChromeDriver();

		// Step 3
		// Utilize the driver and invoke the chrome browser
		// Visit a Page
		driver.get("https://www.google.com");

		// Max the Screen or browser
		driver.manage().window().maximize();

		// Step 4
		// Find a element
		// Creating Webelement onject and by locating it with the driver

		// Search Field
		WebElement searchField = driver.findElement(By.xpath("//input[@title='Search']"));

		// Step 5
		// Work with a Found Element
		searchField.click(); // clicks on element
		// searchField.submit(); // submits a form
		// searchField.clear(); // clears an input field of its text	
		searchField.sendKeys("What is Selenium"); //Types Text into the input field
		
		//Ask A Question 
		//Returns True/False value aka Boolean 
		searchField.isDisplayed(); //Is it Visible to the Human Eye
		searchField.isEnabled(); //can it be selected?
		searchField.isSelected(); //is it Selected?

	}

}
