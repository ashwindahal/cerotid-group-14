package LinerAutomation;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class GoogleFirstTest {

	public static void main(String[] args) {
		// Step 1
		// Setting System Path --> to point to the chromedriver.exe file
		// Windows
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");

		// Step 2
		// Create an instance of Webdriver
		WebDriver driver = new ChromeDriver();

		// Step 3
		// Utilize the driver and invoke the chrome browser navigating to goole website
		driver.get("https://www.google.com");
		// driver.navigate().to(url);

		// Grabbing the Value of title
		String title = driver.getTitle();

		if (title.equals("Google")) {
			System.out.println("Success--------------- Google Page loaded as Expected");
		} else {
			System.out.println(
					"Failed---------------- Google Page is not Loaded as Expected! " + title + " Was launched instead");
		}

		// Creating Webelement obj and pointing to its location
		WebElement searchField = driver.findElement(By.xpath("//input[@name='q']"));

		if (searchField.isDisplayed()) {
			System.out.println("Success---------- Search Field is Displayed");
		} else {
			System.out.println("Failed-------------- Search Field is NOT Displayed");
		}

		// Interacting with the SerachField Webelemnt

		// Sending Keys in the search bar
		searchField.sendKeys("What is Selenium");
		// Pressing enter enter for the given serch
		searchField.sendKeys(Keys.RETURN);

		// Validate we get correct results

		String pageSource = driver.getPageSource();

		if (pageSource.contains("Selenium")) {
			System.out.println("Success----------- Valid Search results were displayed");
		} else {
			System.out.println("Failed------------- Invlaid Serch results were displayed");
		}

		// Close the browser
		driver.close();
		driver.quit();

	}

}
