package LinerAutomation;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class CerotidWebsite {

	static WebDriver driver;

	public static void main(String[] args) {

		// Step 1: Invoke the Browser and navigate to cerotid homepage
		invokeBrowser();
		// Step 2: Fill Form
		fillForm();
		// Step 3: Terminate Browser
		terminateBrowser();

	}

	public static void invokeBrowser() {
		// Step 1: Set the System Path
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		// Step 2: Create an instance of Webdriver of type chrome
		driver = new ChromeDriver();
		// Step 3:
		driver.get("https://www.cerotid.com");
		// Step 4: Maximize the Screen
		driver.manage().window().maximize();

		// Validate if Correct Page is Loaded
		String pageTitle = driver.getTitle();

		if (pageTitle.equals("Cerotid")) {
			System.out.println(pageTitle + " -------------------------------- was launched as expected");
		} else {
			System.out.println("Invalid Page " + pageTitle + " Was launched");
		}

	}

	public static void fillForm() {
		try {

			// Grabbing form Elements
			// Select Course
			WebElement course = driver.findElement(By.xpath("//select[@ id='classType']"));
			// Select Session
			WebElement session = driver.findElement(By.xpath("//select[@ id='sessionType']"));
			// Name
			WebElement name = driver.findElement(By.xpath("//input[@placeholder='Full Name *']"));
			// Address
			WebElement address = driver.findElement(By.xpath("//input[@id='address']"));
			// City
			WebElement city = driver.findElement(By.xpath("//input[@id='city']"));
			// State
			WebElement state = driver.findElement(By.xpath("//select[@id='state']"));
			// Zip Code
			WebElement zip = driver.findElement(By.xpath("//input[@id='zip']"));
			// Email
			WebElement email = driver.findElement(By.xpath("(//input[@id='email'])[1]"));
			// Phone
			WebElement phone = driver.findElement(By.xpath("(//input[@id='phone'])[1]"));

			// Filling Form Here
			WebElement noExcuseMessage = driver.findElement(By.xpath("//*[@id=\"contact\"]/div/div[1]/div/h3"));
			Actions actions = new Actions(driver);
			actions.moveToElement(noExcuseMessage);
			actions.perform();
			
			

			if (course.isDisplayed()) {
				System.out.println("Course is displayed");
				// Creating a select object and passing the web element
				Select selectCourse = new Select(course);
				String myCourse = "QAAutomation";
				selectCourse.selectByValue(myCourse);
				Thread.sleep(2000);
			} else {
				System.out.println("Course is not displayed");
			}

			if (session.isDisplayed()) {
				System.out.println("Seesion is displayed");
				// Creating a select object and passing the web element
				Select selectSession = new Select(session);
				String mySession = "NewSession";
				selectSession.selectByValue(mySession);
				Thread.sleep(2000);
			} else {
				System.out.println("Course is not displayed");
			}

			if (name.isDisplayed() && address.isDisplayed() && city.isDisplayed() && state.isDisplayed()
					&& zip.isDisplayed() && email.isDisplayed() && phone.isDisplayed()) {
				System.out.println("All Expected Elements are Displayed in from");

				name.sendKeys("Test Tester");
				Thread.sleep(2000);

				address.sendKeys("123 Test Address");
				Thread.sleep(2000);

				city.sendKeys("Dallas");
				Thread.sleep(2000);

				// Choose State
				// Create a list to store Web Elements
				List<WebElement> stateList = driver.findElements(By.xpath("//select[@id='state']//option"));

				String stateToChoose = "TX";

				for (int i = 0; i < stateList.size(); i++) {
					if (stateList.get(i).getText().equals(stateToChoose)) {
						stateList.get(i).click();
						break;
					}
				}
				Thread.sleep(2000);

				// Zip Code
				zip.sendKeys("00000");
				Thread.sleep(2000);

				email.sendKeys("someemail@test.com");
				Thread.sleep(2000);

				phone.sendKeys("000-000-0000");
				Thread.sleep(2000);

			} else {
				System.out.println("Not All Expected Elements are Displayed in Form");
			}

		} catch (NoSuchElementException nse) {
			nse.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
			e.getMessage();
		}
	}

	public static void terminateBrowser() {
		System.out.println("Terminating Browser---------------------------");
		// Close the Current Window
		driver.close();
		// Quit Session
		driver.quit();
	}

}
