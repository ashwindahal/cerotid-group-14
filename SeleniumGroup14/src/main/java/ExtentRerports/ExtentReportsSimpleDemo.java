package ExtentRerports;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtentReportsSimpleDemo {
	
	static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {
		// 1: Step 1
		// Create ExtentHtmlReporter Object and create a new Extent Report html File
		ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter("extentReport.html");

		// Step 2
		// Create ExtentReports and attach to the Reporter
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(htmlReporter);

		// Step 3
		// Create a toggle for the given test and add logs under it
		ExtentTest test = extent.createTest("Google Page Test",
				"This Test will check and see if we can perfrom a search from the Google Search Bar");
		
		
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		
		driver = new ChromeDriver();
		
		//step 4
		//Start executing test case and add loggs to provide log events using the ExtentTest Object 
		//LOG Event -- INFO Example 
		test.log(Status.INFO, "Executing Google Page Test");
		
		
		//Start of Test <HERE>
		
		driver.get("https://www.google.com/");
		
		
		if(driver.getTitle().contains("Google")) {
			//LOG EVent -- Pass Example 
			test.pass("Navigated to Google Webpage");
		}else {
			//LOG Event -- Fail Example 
			test.fail("Wrong Page was launched");
		}
		
		driver.manage().window().maximize();
		
		/*
		 * //Fail Example
		 * test.fail("This is a test to check fail message on the Extent Reports UI");
		 */
		
		//INFO Eample 
		test.log(Status.INFO, "Ending Google Home Page Test");
		
		test.warning("DO NOT CONTINUE WITH TEST");

		
		Thread.sleep(3000);
		
		driver.quit();
		
		//Step 5
		//Create flush whcih will write everything to the ExtentHtmlReporter html file 
		extent.flush();

	}

}
