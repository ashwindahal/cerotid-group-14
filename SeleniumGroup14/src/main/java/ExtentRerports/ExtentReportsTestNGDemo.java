package ExtentRerports;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtentReportsTestNGDemo {

	WebDriver driver;
	// 1: Step 1
	// Create ExtentHtmlReporter Object and create a new Extent Report html File
	ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter("extentReportTestNG.html");

	@BeforeTest
	public void beforeTest() {
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	@Test(priority = 0)
	public void GoogleTest() {

		// Step 2
		// Create ExtentReports and attach to the Reporter
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(htmlReporter);

		// Step 3
		// Create a toggle for the given test and add logs under it
		ExtentTest test = extent.createTest("Google Page Test",
				"This Test will check and see if we can perfrom a search from the Google Search Bar");

		// step 4
		// Start executing test case and add loggs to provide log events using the
		// ExtentTest Object
		// LOG Event -- INFO Example
		test.info("Executing Google Test");

		driver.get("https://www.google.com/");

		if (driver.getTitle().contains("Google")) {
			// LOG EVent -- Pass Example
			test.pass("Navigated to Google Webpage");
		} else {
			// LOG Event -- Fail Example
			test.fail("Wrong Page was launched");
		}

		driver.manage().window().maximize();

		// step 4
		// Start executing test case and add loggs to provide log events using the
		// ExtentTest Object
		// LOG Event -- INFO Example
		test.log(Status.INFO, "Ending Google Test");

		ExtentTest test2 = extent.createTest("Yahoo Page Test", "This is a simple Yahoo test");
		test2.info("Executing Yahoo Test....");

		driver.get("https://www.yahoo.com/?guccounter=1");

		if (driver.getTitle().contains("Yahoo")) {
			test2.pass("Navigated to Yahoo homepage");
		}

		driver.manage().window().maximize();

		test2.log(Status.INFO, "Ending Yahoo Test");
		test2.fail("This is a test to check fail status on the Extent Reports UI");

		// Step 5
		// Create flush whcih will write everything to the ExtentHtmlReporter html file
		extent.flush();

	}

	@AfterTest
	public void afterTest() throws InterruptedException {
		Thread.sleep(5000);
		driver.close();
		driver.quit();
	}

}
