package SeleniumUtils;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class ScreenShotUtility {

	public void takeSnopShot(WebDriver driver, String screenShotName) {

		try {

			// Creating a File Object to take Screenshots
			File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

			// Store sourced File in a new file "Screenshots"
			FileUtils.copyFile(src, new File(".\\Screenshots\\" + screenShotName + ".jpeg"));
			
			System.out.println("Screenshot was Taken!!!!!!");

		} catch (Exception e) {
			System.out.println("Exception while taking screenshot" + e.getMessage());
			e.printStackTrace();
		}

	}

}
