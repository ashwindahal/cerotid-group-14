package SeleniumUtils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class GlobalMethods {

	public void ScrollToElement(WebDriver driver, WebElement element) {
		// Filling Form Here
		Actions actions = new Actions(driver);
		actions.moveToElement(element);
		actions.perform();

	}

}
