package DesiredCapabilities;
//1: How to invoke the broser in incognito mode 
//2: How to invoke broser in headless mode 
//3: How to add extensions (We will add a ad blocker to remove adds)

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class DesiredCapabilitiesDemo {

	public static void main(String[] args) {
		
		//Set System path 
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		
		
		//Invoke in Incognito mode 
		//Creating a object of ChromeOptions 
		/*
		 * ChromeOptions options = new ChromeOptions();
		 * options.addArguments("--icognito");
		 */
		
		
		//Invoke in headless mode 
		//Creating a object of ChromeOptions 
		/*
		 * ChromeOptions options = new ChromeOptions();
		 * options.addArguments("--headless");
		 */
		
		//Add Extensions 
		ChromeOptions options = new ChromeOptions();
		options.addExtensions(new File(".\\libs\\extension_4_32_0_0.crx")); 
	
		WebDriver driver = new ChromeDriver(options);
		driver.get("https://www.guru99.com/smoke-testing.html");
		System.out.println(driver.getTitle());
		
	}

}
