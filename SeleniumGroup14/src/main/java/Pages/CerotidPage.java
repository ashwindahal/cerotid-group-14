package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

//Store Webelements and return its value 
public class CerotidPage {

	// Class level variable to store webelements
	private static WebElement element;

	public static WebElement selectCourse(WebDriver driver) {
		// Find the element and return its value
		element = driver.findElement(By.xpath("//select[@id='classType']"));
		return element;
	}

	public static WebElement selectSession(WebDriver driver) {
		// Find the element and return its value
		element = driver.findElement(By.xpath("//select[@id='sessionType']"));
		return element;
	}

	public static WebElement fullName(WebDriver driver) {
		// Find the element and return its value
		element = driver.findElement(By.xpath("//input[@placeholder='Full Name *']"));
		return element;
	}

	public static WebElement addressField(WebDriver driver) {
		// Find the element and return its value
		element = driver.findElement(By.xpath("//input[@placeholder='Address *']"));
		return element;
	}
}
