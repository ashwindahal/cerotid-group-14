package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class CerotidObjectsAndMethods {

	// Create a empty instace of Webdriver
	WebDriver driver;

	// Locating Web Elements using By Object
	By selectCourse = By.xpath("//select[@id='classType']");
	By selectSession = By.xpath("//select[@id='sessionType']");
	By fullName = By.xpath("//input[@placeholder='Full Name *']");
	By addressElement = By.xpath("//input[@placeholder='Address *']");

	// Defualt
	public CerotidObjectsAndMethods() {

	}

	// Overloaded
	public CerotidObjectsAndMethods(WebDriver driver) {
		this.driver = driver;
	}
	
	// Creating a method that lets us interact with elements
	public void selectCourse(int index) {
		WebElement element = driver.findElement(selectCourse);
		Actions action = new Actions(driver);
		action.moveToElement(element);
		Select chooseCourse = new Select(element);
		chooseCourse.selectByIndex(index);
	}

	public void selectSession(String value) {
		WebElement element = driver.findElement(selectSession);
		Select chooseSession = new Select(element);
		chooseSession.selectByValue(value);
	}

	public void enterFullName(String name) {
		WebElement element = driver.findElement(fullName);
		element.sendKeys(name);

	}

	public void enterAddress(String address) {
		driver.findElement(addressElement).sendKeys(address);
	}

}
