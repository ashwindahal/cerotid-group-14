package ActionsClass;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

public class ActionsClassDemo {

	public static void main(String[] args) {
		// clickAndHold() --> Clicks(Without releasing) at the current location
		// doubleClick()
		// contClick --> Performs a conetct-click at the current location --> Right
		// Click
		// dragAndDrop(source,traget)

		// Set the System Path
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");

		// Invoke Browser
		WebDriver driver = new ChromeDriver();
		driver.get("http://automationpractice.com/index.php");

		// Switch to second tab
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));

		// Create an instance of Actions
		Actions action = new Actions(driver);

		WebElement source = driver.findElement(
				By.xpath("//img[@src='http://automationpractice.com/modules/themeconfigurator/img/banner-img6.jpg']"));
		WebElement target = driver.findElement(By.xpath("//input[@name='search_query']"));

		Action contextClick = action.moveToElement(source).clickAndHold().moveToElement(target).release().build();
		contextClick.perform();

		/*
		 * Action doubleClick = action.moveToElement(source).doubleClick().build();
		 * doubleClick.perform();
		 */

	}

}
